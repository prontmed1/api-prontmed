# API ProntMed

<p align="center">
  <a href="https://www.linkedin.com/in/renatoaraujow/" target="_blank" rel="noopener noreferrer">
    <img alt="Made by" src="https://img.shields.io/badge/made%20by-Renato%20Paschoal%20de%20Ara%C3%BAjo-blue">
  </a>
</p>

<p align="center">
  <a href="#%EF%B8%8F-sobre-o-projeto">Sobre o projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-tecnologias-utilizadas">Tecnologias utilizadas</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-como-rodar-a-api">Como rodar a API</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#%EF%B8%8F-como-rodar-os-testes">Como rodar os testes</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <!-- <a href="#%EF%B8%8F-como-ver-os-logs-da-api-no-docker">Como ver os logs da API no docker</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; -->
  <!-- <a href="#-como-parar-a-api">Como parar a API</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; -->
  <a href="#-documentação-e-uso-da-api">Documentação e uso da API</a>
</p>

<!-- <p id="insomniaButton" align="center">
  <a href="https://insomnia.rest/run/?label=ProntMedApi%20-%20renatoraujow&uri=https%3A%2F%2Fgitlab.com%2Fprontmed1%2Fapi-prontmed%2F-%2Fblob%2Fmain%2FInsomnia.json" target="_blank"><img src="https://insomnia.rest/images/run.svg" alt="Run in Insomnia"></a>
</p> -->

## 👨‍⚕️ Sobre o projeto

A API ProntMed foi criada para possibilitar o cadastro e manutenção de médicos, pacientes, agendamentos de consultas e prontuários.

Para ver a **Documentação da API**, clique aqui: [Documentação API](https://renatoaraujo.com.br/api-docs/)

## 💻 Tecnologias utilizadas

Tecnologias que eu utilizei para desenvolver esta API.

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Express](https://expressjs.com/pt-br/)
- [TypeORM](https://typeorm.io/#/)
- [JWT-token](https://jwt.io/)
- [uuid v4](https://github.com/thenativeweb/uuidv4/)
- [PostgreSQL](https://www.postgresql.org/)
- [Day.js](https://day.js.org/)
- [Jest](https://jestjs.io/)
- [SuperTest](https://github.com/visionmedia/supertest)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [EditorConfig](https://editorconfig.org/)
- [Swagger](https://swagger.io/)
- [Git](https://git-scm.com/)
- [Gitlab](https://gitlab.com/)
- [Docker](https://www.docker.com/)

## 🚀 Como rodar a API

### Requisitos para rodar a API

- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/)

> Abra o terminal e execute os seguintes comandos

**Clonar o projeto e acessar a pasta do projeto**

```bash
$ git clone https://gitlab.com/prontmed1/api-prontmed.git

$ cd api-prontmed
```

**Copiar arquivo com as variáveis de ambiente**

```bash
$ cp .env.example .env
```

**Executar Docker-compose com o Node.js e o Postgres**

```bash
$ docker-compose up --build -d
```

**Executar as Migrations para criação do banco de dados**

```bash
$ docker exec prontmed-api npm run typeorm migration:run
```

**API funcionando**

Para ver a API funcionando acesse http://localhost:3333

## ✔️ Como rodar os testes

**Com a API funcionando, execute o comando abaixo para rodar os testes unitários e testes de integração**

```bash
$ docker exec prontmed-api npm run test
```

## ⚙️ Como ver os logs da API no docker

**Execute o comando abaixo**

```bash
$ docker-compose logs
```
> Alternativamente, você pode usar a opção `-f`, assim: `docker-compose logs -f` para ficar acompanhando os logs gerados pela API.
> Se quiser parar de acompanhar os logs, no terminal aperte `CTRL+C`.

## 🛑 Como parar a API

**Execute o comando abaixo**

```bash
$ docker-compose stop
```
> Para rodar a API novamente, basta executar o comando: `docker-compose up -d`

## 📝 Documentação e uso da API

Para usar e testar os endpoints da API na sua máquina acesse a documentação localhost: [Documentação Localhost](http://localhost:3333/api-docs)
<br>
Para usar e testar os endpoints da API na cloud acesse a documentação da cloud: [Documentação Cloud](https://renatoaraujo.com.br/api-docs/)

> Alternativamente, você pode testar os endpoints através da aplicação [Insomnia REST](https://insomnia.rest/download).<br>
> Depois de baixar o Insomnia REST, importe o arquivo [Insomnia.json](https://gitlab.com/prontmed1/api-prontmed/-/blob/main/Insomnia.json) que se encontra na raiz deste projeto.<br>
> No insomnia já estão configurados os dois ambientes: Localhost e Cloud.

**Diagrama ER**

O Diagrama ER, pode ser visualizado aqui: [Diagrama ER](https://gitlab.com/prontmed1/api-prontmed/-/blob/main/diagrama-er.jpeg)

**Conexão com o banco de dados**

Para conectar ao banco de dados local através de uma IDE externa, com a API rodando, você pode utilizar as seguintes configurações:<br>
Host: localhost<br>
Database: prontmed<br>
Username: prontmed<br>
Password: 123456


---

Feito por Renato Paschoal de Araújo 👋 &nbsp;[Ver Linkedin](https://www.linkedin.com/in/renatoaraujow/)
