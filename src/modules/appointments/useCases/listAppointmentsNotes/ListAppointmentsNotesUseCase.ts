import { inject, injectable } from "tsyringe";

import { IListAppointmentsNotesDTO } from "@modules/appointments/dtos/IListAppointmentsNotesDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ListAppointmentsNotesUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository,
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository
  ) {}

  async execute({
    doctor_id,
    patient_id,
    offset,
    limit,
  }: IListAppointmentsNotesDTO): Promise<Appointment[]> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
      active: true,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    return this.appointmentsRepository.findByPatientId({
      doctor_id,
      patient_id,
      offset,
      limit,
    });
  }
}

export { ListAppointmentsNotesUseCase };
