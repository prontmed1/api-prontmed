import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListAppointmentsNotesUseCase } from "./ListAppointmentsNotesUseCase";

class ListAppointmentsNotesController {
  async handle(request: Request, response: Response): Promise<Response> {
    const patient_id = request.query.patient_id as string;
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;
    const { id: doctor_id } = request.doctor;

    const listAppointmentsNotesUseCase = container.resolve(
      ListAppointmentsNotesUseCase
    );

    const appointments = await listAppointmentsNotesUseCase.execute({
      doctor_id,
      patient_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(appointments);
  }
}

export { ListAppointmentsNotesController };
