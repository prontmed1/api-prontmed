import { v4 as uuidV4 } from "uuid";

import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { AppointmentsRepositoryInMemory } from "@modules/appointments/repositories/in-memory/AppointmentsRepositoryInMemory";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { CreatePatientUseCase } from "@modules/patients/useCases/createPatient/CreatePatientUseCase";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { CreateAppointmentUseCase } from "../createAppointment/CreateAppointmentUseCase";
import { ListAppointmentsNotesUseCase } from "./ListAppointmentsNotesUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let appointmentsRepositoryInMemory: AppointmentsRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let createAppointmentUseCase: CreateAppointmentUseCase;
let listAppointmentsNotesUseCase: ListAppointmentsNotesUseCase;

let doctorId: string;
let patientId: string;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;
let appointment: Appointment;

describe("List Appointments Notes", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    appointmentsRepositoryInMemory = new AppointmentsRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    createAppointmentUseCase = new CreateAppointmentUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );
    listAppointmentsNotesUseCase = new ListAppointmentsNotesUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    const patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    patientId = patient.id;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );

    appointment = await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
      notes: "Notes Test",
    });
  });

  it("should be able to list appointments notes", async () => {
    const appointments = await listAppointmentsNotesUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
    });

    expect(appointments).toEqual([appointment]);
  });

  it("should not be able to list appointments notes to a non-existent doctor", async () => {
    await expect(
      listAppointmentsNotesUseCase.execute({
        doctor_id: uuidV4(),
        patient_id: patientId,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not be able to list appointments notes to a non-existent patient", async () => {
    await expect(
      listAppointmentsNotesUseCase.execute({
        doctor_id: doctorId,
        patient_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });

  it("should be able to list appointments with offset and limit", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    const appointmentDateTwo = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 12:00:00`
    );

    const appointmentTwo = await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
      notes: "Notes Test Two",
    });

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDateTwo,
      notes: "Notes Test Three",
    });

    const appointments = await listAppointmentsNotesUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      offset: 1,
      limit: 1,
    });

    expect(appointments).toEqual([appointmentTwo]);
    expect(appointments.length).toBe(1);
  });
});
