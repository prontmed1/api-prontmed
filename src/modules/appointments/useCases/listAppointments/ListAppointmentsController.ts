import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListAppointmentsUseCase } from "./ListAppointmentsUseCase";

class ListAppointmentsController {
  async handle(request: Request, response: Response): Promise<Response> {
    const patient_id = request.query.patient_id as string;
    const start_date = request.query.start_date as string;
    const end_date = request.query.end_date as string;
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;

    const { id: doctor_id } = request.doctor;

    const listAppointmentsUseCase = container.resolve(ListAppointmentsUseCase);

    const appointments = await listAppointmentsUseCase.execute({
      doctor_id,
      start_date: new Date(start_date),
      end_date: new Date(end_date),
      patient_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(appointments);
  }
}

export { ListAppointmentsController };
