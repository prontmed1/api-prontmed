import { inject, injectable } from "tsyringe";

import { IListAppointmentsDTO } from "@modules/appointments/dtos/IListAppointmentsDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { IDateProvider } from "@shared/container/providers/DateProvider/IDateProvider";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ListAppointmentsUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository,
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute({
    doctor_id,
    start_date,
    end_date,
    patient_id,
    offset,
    limit,
  }: IListAppointmentsDTO): Promise<Appointment[]> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    if (patient_id) {
      const patient = await this.patientsRepository.findByIdAndDoctorId({
        patient_id,
        doctor_id,
        active: true,
      });

      if (!patient) {
        throw new AppError("Patient not found");
      }
    }

    const startDate = start_date.toISOString();
    const endDate = this.dateProvider.addDaysToDate(end_date, 1).toISOString();

    return this.appointmentsRepository.findByDateAndPatientId({
      doctor_id,
      patient_id,
      start_date: startDate,
      end_date: endDate,
      offset,
      limit,
    });
  }
}

export { ListAppointmentsUseCase };
