import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("List Appointments Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctors(id, name, email, password, appointment_duration, created_at, updated_at, active)
        values('${id}', 'Doctor Test', 'doctor@example.com', '${password}', 29, 'now()', 'now()', true)`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to list appointments", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );
    const appointmentListDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    ).toISOString();

    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "doctor@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patients")
      .send({
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patient@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { body: patients } = await request(app)
      .get("/patients")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    await request(app)
      .post("/appointments")
      .send({
        patient_id: patients[0].id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .get(`/appointments`)
      .query({
        start_date: appointmentListDate,
        end_date: appointmentListDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(1);
    expect(response.body[0]).toHaveProperty("id");
    expect(response.body[0]).toHaveProperty("patient");
  });
});
