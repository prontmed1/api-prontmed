import { v4 as uuidV4 } from "uuid";

import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { AppointmentsRepositoryInMemory } from "@modules/appointments/repositories/in-memory/AppointmentsRepositoryInMemory";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { CreatePatientUseCase } from "@modules/patients/useCases/createPatient/CreatePatientUseCase";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { CreateAppointmentUseCase } from "../createAppointment/CreateAppointmentUseCase";
import { ListAppointmentsUseCase } from "./ListAppointmentsUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let appointmentsRepositoryInMemory: AppointmentsRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let createAppointmentUseCase: CreateAppointmentUseCase;
let listAppointmentsUseCase: ListAppointmentsUseCase;

let doctorId: string;
let patientId: string;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;
let appointment: Appointment;

describe("List Appointments", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    appointmentsRepositoryInMemory = new AppointmentsRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    createAppointmentUseCase = new CreateAppointmentUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );
    listAppointmentsUseCase = new ListAppointmentsUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    const patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    patientId = patient.id;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );

    appointment = await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });
  });

  it("should be able to list appointments", async () => {
    const appointmentStartDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    const appointmentEndDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    const appointments = await listAppointmentsUseCase.execute({
      doctor_id: doctorId,
      start_date: appointmentStartDate,
      end_date: appointmentEndDate,
    });

    expect(appointments).toEqual([appointment]);
  });

  it("should not be able to list appointments to a non-existent doctor", async () => {
    const appointmentStartDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    const appointmentEndDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    await expect(
      listAppointmentsUseCase.execute({
        doctor_id: uuidV4(),
        start_date: appointmentStartDate,
        end_date: appointmentEndDate,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should be able to list appointments with offset and limit by patient", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    const appointmentDateTwo = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 12:00:00`
    );

    const appointmentStartDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    const appointmentEndDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay}`
    );

    const appointmentTwo = await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDateTwo,
    });

    const appointments = await listAppointmentsUseCase.execute({
      doctor_id: doctorId,
      start_date: appointmentStartDate,
      end_date: appointmentEndDate,
      patient_id: patientId,
      offset: 1,
      limit: 1,
    });

    expect(appointments).toEqual([appointmentTwo]);
    expect(appointments.length).toBe(1);
  });
});
