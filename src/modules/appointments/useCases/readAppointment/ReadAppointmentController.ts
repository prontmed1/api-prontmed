import { Request, Response } from "express";
import { container } from "tsyringe";

import { ReadAppointmentUseCase } from "./ReadAppointmentUseCase";

class ReadAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { appointment_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const readAppointmentUseCase = container.resolve(ReadAppointmentUseCase);

    const appointment = await readAppointmentUseCase.execute({
      appointment_id,
      doctor_id,
    });

    return response.json(appointment);
  }
}

export { ReadAppointmentController };
