import { inject, injectable } from "tsyringe";

import { IReadAppointmentDTO } from "@modules/appointments/dtos/IReadAppointmentDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ReadAppointmentNoteUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    appointment_id,
    doctor_id,
  }: IReadAppointmentDTO): Promise<Appointment> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const appointment = await this.appointmentsRepository.findNoteById({
      appointment_id,
      doctor_id,
    });

    if (!appointment) {
      throw new AppError("Appointment not found");
    }

    return appointment;
  }
}

export { ReadAppointmentNoteUseCase };
