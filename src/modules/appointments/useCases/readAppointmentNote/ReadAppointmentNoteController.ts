import { Request, Response } from "express";
import { container } from "tsyringe";

import { ReadAppointmentNoteUseCase } from "./ReadAppointmentNoteUseCase";

class ReadAppointmentNoteController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { appointment_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const readAppointmentNoteUseCase = container.resolve(
      ReadAppointmentNoteUseCase
    );

    const appointment = await readAppointmentNoteUseCase.execute({
      appointment_id,
      doctor_id,
    });

    return response.json(appointment);
  }
}

export { ReadAppointmentNoteController };
