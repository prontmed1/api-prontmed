import { inject, injectable } from "tsyringe";

import { IDeleteAppointmentDTO } from "@modules/appointments/dtos/IDeleteAppointmentDTO";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class DeleteAppointmentUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository,
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository
  ) {}

  async execute({
    appointment_id,
    doctor_id,
  }: IDeleteAppointmentDTO): Promise<void> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const appointment = await this.appointmentsRepository.findById({
      appointment_id,
      doctor_id,
    });

    if (!appointment) {
      throw new AppError("Appointment not found");
    }

    this.appointmentsRepository.delete(appointment_id);
  }
}

export { DeleteAppointmentUseCase };
