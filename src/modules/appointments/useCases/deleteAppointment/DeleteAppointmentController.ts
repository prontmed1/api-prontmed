import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeleteAppointmentUseCase } from "./DeleteAppointmentUseCase";

class DeleteAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { appointment_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const deleteAppointmentUseCase = container.resolve(
      DeleteAppointmentUseCase
    );

    await deleteAppointmentUseCase.execute({
      appointment_id,
      doctor_id,
    });

    return response.status(204).send();
  }
}

export { DeleteAppointmentController };
