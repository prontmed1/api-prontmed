import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdateAppointmentNotesUseCase } from "./UpdateAppointmentNotesUseCase";

class UpdateAppointmentNotesController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { notes } = request.body;
    const { appointment_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const updateAppointmentNotesUseCase = container.resolve(
      UpdateAppointmentNotesUseCase
    );

    await updateAppointmentNotesUseCase.execute({
      appointment_id,
      doctor_id,
      notes,
    });

    return response.status(204).send();
  }
}

export { UpdateAppointmentNotesController };
