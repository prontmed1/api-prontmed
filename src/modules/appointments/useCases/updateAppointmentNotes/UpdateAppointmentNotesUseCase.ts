import { inject, injectable } from "tsyringe";

import { IUpdateAppointmentNotesDTO } from "@modules/appointments/dtos/IUpdateAppointmentNotesDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class UpdateAppointmentNotesUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    appointment_id,
    doctor_id,
    notes,
  }: IUpdateAppointmentNotesDTO): Promise<Appointment> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const appointment = await this.appointmentsRepository.findById({
      appointment_id,
      doctor_id,
    });

    if (!appointment) {
      throw new AppError("Appointment not found");
    }

    Object.assign(appointment, {
      notes,
    });

    this.appointmentsRepository.save(appointment);

    return appointment;
  }
}

export { UpdateAppointmentNotesUseCase };
