import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdateAppointmentUseCase } from "./UpdateAppointmentUseCase";

class UpdateAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id, date, notes } = request.body;
    const { appointment_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const updateAppointmentUseCase = container.resolve(
      UpdateAppointmentUseCase
    );

    await updateAppointmentUseCase.execute({
      appointment_id,
      doctor_id,
      patient_id,
      date,
      notes,
    });

    return response.status(204).send();
  }
}

export { UpdateAppointmentController };
