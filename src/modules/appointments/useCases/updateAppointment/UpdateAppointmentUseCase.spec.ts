import { v4 as uuidV4 } from "uuid";

import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { AppointmentsRepositoryInMemory } from "@modules/appointments/repositories/in-memory/AppointmentsRepositoryInMemory";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { CreatePatientUseCase } from "@modules/patients/useCases/createPatient/CreatePatientUseCase";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { CreateAppointmentUseCase } from "../createAppointment/CreateAppointmentUseCase";
import { UpdateAppointmentUseCase } from "./UpdateAppointmentUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let appointmentsRepositoryInMemory: AppointmentsRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let createAppointmentUseCase: CreateAppointmentUseCase;
let updateAppointmentUseCase: UpdateAppointmentUseCase;

let doctorId: string;
let doctorAppointmentDuration: number;
let patientId: string;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;
let appointment: Appointment;

describe("Update Appointment", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    appointmentsRepositoryInMemory = new AppointmentsRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    createAppointmentUseCase = new CreateAppointmentUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );
    updateAppointmentUseCase = new UpdateAppointmentUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;
    doctorAppointmentDuration = doctor.appointment_duration;

    const patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    patientId = patient.id;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );

    appointment = await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });
  });

  it("should be able to update an appointment", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    const appointmentUpdated = await updateAppointmentUseCase.execute({
      appointment_id: appointment.id,
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });

    const appointmentFound = await appointmentsRepositoryInMemory.findById({
      appointment_id: appointment.id,
      doctor_id: doctorId,
    });

    expect(appointmentFound.date).toBe(appointmentUpdated.date);
  });

  it("should not be able to update an appointment on a past date", async () => {
    const appointmentDate = new Date(
      `${currentYear - 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: appointment.id,
        doctor_id: doctorId,
        patient_id: patientId,
        date: appointmentDate,
      })
    ).rejects.toEqual(
      new AppError("You can't create an appointment on past date")
    );
  });

  it("should not be able to update an appointment if there is another one with the same date and time", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: appointment.id,
        doctor_id: doctorId,
        patient_id: patientId,
        date: appointmentDate,
      })
    ).rejects.toEqual(
      new AppError("The chosen date and time are not available")
    );
  });

  it("should be able to update an appointment with the same date if it is a different doctor", async () => {
    const doctorTwo = await createDoctorUseCase.execute({
      name: "Doctor Test Two",
      email: "doctor2@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    const patientTwo = await createPatientUseCase.execute({
      doctor_id: doctorTwo.id,
      name: "Patient Test Two",
      phone: "+5548999669966",
      email: "patient2@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await createAppointmentUseCase.execute({
      doctor_id: doctorTwo.id,
      patient_id: patientTwo.id,
      date: appointmentDate,
    });

    const appointmentUpdated = await updateAppointmentUseCase.execute({
      appointment_id: appointment.id,
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });

    const appointmentFound = await appointmentsRepositoryInMemory.findById({
      appointment_id: appointment.id,
      doctor_id: doctorId,
    });

    expect(appointmentFound.date).toBe(appointmentUpdated.date);
  });

  it("should not be able to update an appointment if there is another one with the same date interval", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    const appointmentDateNew = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:45:00`
    );

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patientId,
      date: appointmentDate,
    });

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: appointment.id,
        doctor_id: doctorId,
        patient_id: patientId,
        date: appointmentDateNew,
      })
    ).rejects.toEqual(
      new AppError(
        `The appointment must respect the interval of ${doctorAppointmentDuration} minutes between then`
      )
    );
  });

  it("should not be able to update an appointment to a non-existent doctor", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: appointment.id,
        doctor_id: uuidV4(),
        patient_id: patientId,
        date: appointmentDate,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not be able to update an appointment to a non-existent patient", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: appointment.id,
        doctor_id: doctorId,
        patient_id: uuidV4(),
        date: appointmentDate,
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });

  it("should not be able to update a non-existent appointment", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await expect(
      updateAppointmentUseCase.execute({
        appointment_id: uuidV4(),
        doctor_id: doctorId,
        patient_id: patientId,
        date: appointmentDate,
      })
    ).rejects.toEqual(new AppError("Appointment not found"));
  });
});
