import { inject, injectable } from "tsyringe";

import { IUpdateAppointmentDTO } from "@modules/appointments/dtos/IUpdateAppointmentDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { IDateProvider } from "@shared/container/providers/DateProvider/IDateProvider";
import { AppError } from "@shared/errors/AppError";

@injectable()
class UpdateAppointmentUseCase {
  constructor(
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository,
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute({
    appointment_id,
    doctor_id,
    patient_id,
    date,
    notes,
  }: IUpdateAppointmentDTO): Promise<Appointment> {
    const currentDate = this.dateProvider.currentDate();

    if (this.dateProvider.compareIfBefore(date, currentDate)) {
      throw new AppError("You can't create an appointment on past date");
    }

    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
      active: true,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    const appointment = await this.appointmentsRepository.findById({
      appointment_id,
      doctor_id,
    });

    if (!appointment) {
      throw new AppError("Appointment not found");
    }

    const appointmentInSameDate = await this.appointmentsRepository.findByDate({
      date,
      doctor_id,
    });

    if (appointmentInSameDate && appointment_id !== appointmentInSameDate.id) {
      throw new AppError("The chosen date and time are not available");
    }

    const dateLess = this.dateProvider
      .subtractMinutesToDate(date, doctor.appointment_duration)
      .toISOString();

    const dateGreater = this.dateProvider
      .addMinutesToDate(date, doctor.appointment_duration)
      .toISOString();

    const appointmentInSameInterval =
      await this.appointmentsRepository.findByInterval({
        doctor_id,
        date_less: dateLess,
        date_greater: dateGreater,
        appointment_id,
      });

    if (appointmentInSameInterval.length) {
      throw new AppError(
        `The appointment must respect the interval of ${doctor.appointment_duration} minutes between then`
      );
    }

    Object.assign(appointment, {
      patient_id,
      date,
      notes,
    });

    this.appointmentsRepository.save(appointment);

    return appointment;
  }
}

export { UpdateAppointmentUseCase };
