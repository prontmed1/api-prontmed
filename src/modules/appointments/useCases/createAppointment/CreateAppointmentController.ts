import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateAppointmentUseCase } from "./CreateAppointmentUseCase";

class CreateAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id, date, notes } = request.body;
    const { id: doctor_id } = request.doctor;

    const createAppointmentUseCase = container.resolve(
      CreateAppointmentUseCase
    );

    await createAppointmentUseCase.execute({
      doctor_id,
      patient_id,
      date,
      notes,
    });

    return response.status(201).send();
  }
}

export { CreateAppointmentController };
