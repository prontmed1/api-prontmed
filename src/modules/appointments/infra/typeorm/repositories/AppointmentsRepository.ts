import {
  Between,
  FindManyOptions,
  getRepository,
  Not,
  Repository,
} from "typeorm";

import { ICreateAppointmentDTO } from "@modules/appointments/dtos/ICreateAppointmentDTO";
import { IFindByDateAndPatientIdDTO } from "@modules/appointments/dtos/IFindByDateAndPatientIdDTO";
import { IFindByDateDTO } from "@modules/appointments/dtos/IFindByDateDTO";
import { IFindByIdDTO } from "@modules/appointments/dtos/IFindByIdDTO";
import { IFindByIntervalDTO } from "@modules/appointments/dtos/IFindByIntervalDTO";
import { IFindByPatientIdDTO } from "@modules/appointments/dtos/IFindByPatientIdDTO";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";

import { Appointment } from "../entities/Appointment";

class AppointmentsRepository implements IAppointmentsRepository {
  private repository: Repository<Appointment>;

  constructor() {
    this.repository = getRepository(Appointment);
  }

  async create({
    doctor_id,
    patient_id,
    date,
    notes,
  }: ICreateAppointmentDTO): Promise<Appointment> {
    const appointment = this.repository.create({
      doctor_id,
      patient_id,
      date,
      notes,
    });

    await this.repository.save(appointment);

    return appointment;
  }

  async save(appointment: Appointment): Promise<void> {
    this.repository.save(appointment);
  }

  async findByDate({ doctor_id, date }: IFindByDateDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne({
      select: ["id"],
      where: {
        doctor_id,
        active: true,
        date,
      },
    });

    return appointment;
  }

  async findByInterval({
    doctor_id,
    date_less,
    date_greater,
    appointment_id,
  }: IFindByIntervalDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      active: true,
      date: Between(date_less, date_greater),
    };

    if (appointment_id) {
      Object.assign(where, { id: Not(appointment_id) });
    }

    const appointments = await this.repository.find({
      select: ["id"],
      where,
    });

    return appointments;
  }

  async deleteAppointmentsNotesByPatientId(patient_id: string): Promise<void> {
    this.repository.update({ patient_id }, { notes: null });
  }

  async findByDateAndPatientId({
    doctor_id,
    start_date,
    end_date,
    patient_id,
    offset,
    limit,
  }: IFindByDateAndPatientIdDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      date: Between(start_date, end_date),
      active: true,
      patient: {
        active: true,
      },
    };

    if (patient_id) {
      Object.assign(where, { patient_id });
    }

    const findOptions = {
      select: ["id", "date", "notes", "created_at", "updated_at"],
      relations: ["patient"],
      where,
      order: { date: "ASC" },
    } as FindManyOptions;

    if (offset) {
      Object.assign(findOptions, { skip: offset });
    }

    if (limit) {
      Object.assign(findOptions, { take: limit });
    }

    const appointments = await this.repository.find(findOptions);

    return appointments;
  }

  async findById({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne(appointment_id, {
      where: {
        doctor_id,
        active: true,
      },
    });

    return appointment;
  }

  async findByIdWithPatient({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne(appointment_id, {
      relations: ["patient"],
      where: {
        doctor_id,
        active: true,
        patient: {
          active: true,
        },
      },
    });

    return appointment;
  }

  async findNoteById({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne(appointment_id, {
      select: ["id", "date", "notes", "created_at", "updated_at"],
      where: {
        doctor_id,
        active: true,
      },
    });

    return appointment;
  }

  async delete(appointment_id: string): Promise<void> {
    this.repository.update({ id: appointment_id }, { active: false });
  }

  async findByPatientId({
    patient_id,
    doctor_id,
    offset,
    limit,
  }: IFindByPatientIdDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      patient_id,
      active: true,
    };

    const findOptions = {
      select: ["id", "date", "notes", "created_at", "updated_at"],
      where,
      order: { date: "DESC" },
    } as FindManyOptions;

    if (offset) {
      Object.assign(findOptions, { skip: offset });
    }

    if (limit) {
      Object.assign(findOptions, { take: limit });
    }

    const appointments = await this.repository.find(findOptions);

    return appointments;
  }
}

export { AppointmentsRepository };
