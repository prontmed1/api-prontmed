interface IFindByIntervalDTO {
  doctor_id: string;
  date_less: string;
  date_greater: string;
  appointment_id?: string;
}

export { IFindByIntervalDTO };
