interface IFindByDateAndPatientIdDTO {
  doctor_id: string;
  start_date: string;
  end_date: string;
  patient_id: string;
  offset: number;
  limit: number;
}

export { IFindByDateAndPatientIdDTO };
