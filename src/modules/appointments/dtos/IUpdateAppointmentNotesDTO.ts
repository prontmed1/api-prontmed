interface IUpdateAppointmentNotesDTO {
  appointment_id: string;
  doctor_id: string;
  notes: string;
}

export { IUpdateAppointmentNotesDTO };
