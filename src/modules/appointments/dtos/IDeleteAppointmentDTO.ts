interface IDeleteAppointmentDTO {
  appointment_id: string;
  doctor_id: string;
}

export { IDeleteAppointmentDTO };
