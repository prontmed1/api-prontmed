interface IFindByIdDTO {
  appointment_id: string;
  doctor_id: string;
}

export { IFindByIdDTO };
