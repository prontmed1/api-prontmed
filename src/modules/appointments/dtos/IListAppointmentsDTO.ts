interface IListAppointmentsDTO {
  doctor_id: string;
  start_date: Date;
  end_date: Date;
  patient_id?: string;
  offset?: number;
  limit?: number;
}

export { IListAppointmentsDTO };
