interface ICreateAppointmentDTO {
  doctor_id: string;
  patient_id: string;
  date: Date;
  notes?: string;
}

export { ICreateAppointmentDTO };
