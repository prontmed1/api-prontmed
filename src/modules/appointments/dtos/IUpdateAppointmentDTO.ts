interface IUpdateAppointmentDTO {
  appointment_id: string;
  doctor_id: string;
  patient_id: string;
  date: Date;
  notes?: string;
}

export { IUpdateAppointmentDTO };
