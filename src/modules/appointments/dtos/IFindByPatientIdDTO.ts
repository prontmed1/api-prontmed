interface IFindByPatientIdDTO {
  patient_id: string;
  doctor_id: string;
  offset?: number;
  limit?: number;
}

export { IFindByPatientIdDTO };
