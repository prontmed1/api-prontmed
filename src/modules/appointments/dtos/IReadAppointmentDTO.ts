interface IReadAppointmentDTO {
  appointment_id: string;
  doctor_id: string;
}

export { IReadAppointmentDTO };
