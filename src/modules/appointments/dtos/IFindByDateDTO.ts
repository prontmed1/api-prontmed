interface IFindByDateDTO {
  doctor_id: string;
  date: Date;
}

export { IFindByDateDTO };
