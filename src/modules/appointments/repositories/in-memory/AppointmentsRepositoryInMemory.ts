import { ICreateAppointmentDTO } from "@modules/appointments/dtos/ICreateAppointmentDTO";
import { IFindByDateAndPatientIdDTO } from "@modules/appointments/dtos/IFindByDateAndPatientIdDTO";
import { IFindByDateDTO } from "@modules/appointments/dtos/IFindByDateDTO";
import { IFindByIdDTO } from "@modules/appointments/dtos/IFindByIdDTO";
import { IFindByIntervalDTO } from "@modules/appointments/dtos/IFindByIntervalDTO";
import { IFindByPatientIdDTO } from "@modules/appointments/dtos/IFindByPatientIdDTO";
import { Appointment } from "@modules/appointments/infra/typeorm/entities/Appointment";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";

import { IAppointmentsRepository } from "../IAppointmentsRepository";

class AppointmentsRepositoryInMemory implements IAppointmentsRepository {
  appointments: Appointment[] = [];
  private dateProvider: DayjsDateProvider = new DayjsDateProvider();

  async create({
    doctor_id,
    patient_id,
    date,
    notes,
  }: ICreateAppointmentDTO): Promise<Appointment> {
    const appointment = new Appointment();

    Object.assign(appointment, {
      doctor_id,
      patient_id,
      date: this.dateProvider.convertToTimezone(date),
      notes,
    });

    this.appointments.push(appointment);

    return appointment;
  }

  async findById({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.id === appointment_id
    );

    return appointment;
  }

  async findByIdWithPatient({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.id === appointment_id
    );

    return appointment;
  }

  async findNoteById({
    appointment_id,
    doctor_id,
  }: IFindByIdDTO): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.id === appointment_id
    );

    return appointment;
  }

  async findByDate({ doctor_id, date }: IFindByDateDTO): Promise<Appointment> {
    const findDate = this.dateProvider.convertToTimezone(date);

    const appointment = this.appointments.find(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.date.getTime() === findDate.getTime()
    );

    return appointment;
  }

  async findByInterval({
    doctor_id,
    date_less,
    date_greater,
  }: IFindByIntervalDTO): Promise<Appointment[]> {
    const appointments = this.appointments.filter(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.date >= new Date(date_less) &&
        appointmentInMemory.date <= new Date(date_greater)
    );

    return appointments;
  }

  async deleteAppointmentsNotesByPatientId(patient_id: string): Promise<void> {
    const appointments = this.appointments.filter(
      (appointmentInMemory) => appointmentInMemory.patient_id === patient_id
    );

    appointments.forEach((appointment, index) => {
      this.appointments[index].notes = null;
    });
  }

  async findByDateAndPatientId({
    doctor_id,
    patient_id,
    start_date,
    end_date,
    offset,
    limit,
  }: IFindByDateAndPatientIdDTO): Promise<Appointment[]> {
    let appointments = this.appointments.filter(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.date >= new Date(start_date) &&
        appointmentInMemory.date <= new Date(end_date)
    );

    if (patient_id) {
      appointments = appointments.filter(
        (appointment) => appointment.patient_id === patient_id
      );
    }

    if (offset && limit) {
      return appointments.slice(offset, limit + 1);
    }

    return appointments;
  }

  async save(appointment: Appointment): Promise<void> {
    const findIndex = this.appointments.findIndex(
      (appointmentInMemory) => appointmentInMemory.id === appointment.id
    );

    this.appointments[findIndex] = appointment;
  }

  async delete(appointment_id: string): Promise<void> {
    const appointment = this.appointments.find(
      (appointmentInMemory) => appointmentInMemory.id === appointment_id
    );

    this.appointments.splice(this.appointments.indexOf(appointment));
  }

  async findByPatientId({
    doctor_id,
    patient_id,
    offset,
    limit,
  }: IFindByPatientIdDTO): Promise<Appointment[]> {
    const appointments = this.appointments.filter(
      (appointmentInMemory) =>
        appointmentInMemory.doctor_id === doctor_id &&
        appointmentInMemory.patient_id === patient_id
    );

    if (offset && limit) {
      return appointments.slice(offset, limit + 1);
    }

    return appointments;
  }
}

export { AppointmentsRepositoryInMemory };
