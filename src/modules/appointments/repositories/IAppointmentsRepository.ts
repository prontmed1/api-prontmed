import { ICreateAppointmentDTO } from "../dtos/ICreateAppointmentDTO";
import { IFindByDateAndPatientIdDTO } from "../dtos/IFindByDateAndPatientIdDTO";
import { IFindByDateDTO } from "../dtos/IFindByDateDTO";
import { IFindByIdDTO } from "../dtos/IFindByIdDTO";
import { IFindByIntervalDTO } from "../dtos/IFindByIntervalDTO";
import { IFindByPatientIdDTO } from "../dtos/IFindByPatientIdDTO";
import { Appointment } from "../infra/typeorm/entities/Appointment";

interface IAppointmentsRepository {
  create(data: ICreateAppointmentDTO): Promise<Appointment>;
  findById(date: IFindByIdDTO): Promise<Appointment>;
  findByIdWithPatient(date: IFindByIdDTO): Promise<Appointment>;
  findNoteById(date: IFindByIdDTO): Promise<Appointment>;
  findByDate(data: IFindByDateDTO): Promise<Appointment>;
  findByInterval(data: IFindByIntervalDTO): Promise<Appointment[]>;
  deleteAppointmentsNotesByPatientId(patient_id: string): Promise<void>;
  findByDateAndPatientId(
    data: IFindByDateAndPatientIdDTO
  ): Promise<Appointment[]>;
  save(appointment: Appointment): Promise<void>;
  delete(appointment_id: string): Promise<void>;
  findByPatientId(data: IFindByPatientIdDTO): Promise<Appointment[]>;
}

export { IAppointmentsRepository };
