import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeletePatientLgpdUseCase } from "./DeletePatientLgpdUseCase";

class DeletePatientLgpdController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const deletePatientLgpdUseCase = container.resolve(
      DeletePatientLgpdUseCase
    );

    await deletePatientLgpdUseCase.execute({
      patient_id,
      doctor_id,
    });

    return response.status(204).send();
  }
}

export { DeletePatientLgpdController };
