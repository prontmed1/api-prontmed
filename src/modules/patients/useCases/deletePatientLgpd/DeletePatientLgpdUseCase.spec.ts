import { v4 as uuidV4 } from "uuid";

import { AppointmentsRepositoryInMemory } from "@modules/appointments/repositories/in-memory/AppointmentsRepositoryInMemory";
import { CreateAppointmentUseCase } from "@modules/appointments/useCases/createAppointment/CreateAppointmentUseCase";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { CreatePatientUseCase } from "../createPatient/CreatePatientUseCase";
import { DeletePatientLgpdUseCase } from "./DeletePatientLgpdUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let appointmentsRepositoryInMemory: AppointmentsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let createAppointmentUseCase: CreateAppointmentUseCase;
let dateProvider: DayjsDateProvider;
let deletePatientLgpdUseCase: DeletePatientLgpdUseCase;

let doctorId: string;
let patient: Patient;

describe("Delete Patient LGPD", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    appointmentsRepositoryInMemory = new AppointmentsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    createAppointmentUseCase = new CreateAppointmentUseCase(
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory,
      patientsRepositoryInMemory,
      dateProvider
    );
    deletePatientLgpdUseCase = new DeletePatientLgpdUseCase(
      patientsRepositoryInMemory,
      appointmentsRepositoryInMemory,
      doctorsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to delete patients to lgpd", async () => {
    expect(
      await deletePatientLgpdUseCase.execute({
        doctor_id: patient.doctor_id,
        patient_id: patient.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("should be able to delete appointments notes to patients to lgpd", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );
    const appointmentDateTwo = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:30:00`
    );

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patient.id,
      date: appointmentDate,
      notes: "Test Notes One",
    });

    await createAppointmentUseCase.execute({
      doctor_id: doctorId,
      patient_id: patient.id,
      date: appointmentDateTwo,
      notes: "Test Notes Two",
    });

    await deletePatientLgpdUseCase.execute({
      doctor_id: patient.doctor_id,
      patient_id: patient.id,
    });

    const appointments = await appointmentsRepositoryInMemory.findByPatientId({
      doctor_id: patient.doctor_id,
      patient_id: patient.id,
    });

    appointments.forEach((appointment) => {
      expect(appointment.notes).toBeNull();
    });
  });

  it("should not be able to delete patients to lgpd to a non-existent doctor", async () => {
    await expect(
      deletePatientLgpdUseCase.execute({
        doctor_id: uuidV4(),
        patient_id: patient.id,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not be able to delete non-existent patients to lgpd", async () => {
    await expect(
      deletePatientLgpdUseCase.execute({
        doctor_id: doctorId,
        patient_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});
