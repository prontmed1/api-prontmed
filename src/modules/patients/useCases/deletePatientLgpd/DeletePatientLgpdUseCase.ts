import { inject, injectable } from "tsyringe";

import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IDeletePatientDTO } from "@modules/patients/dtos/IDeletePatientDTO";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class DeletePatientLgpdUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("AppointmentsRepository")
    private appointmentsRepository: IAppointmentsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({ patient_id, doctor_id }: IDeletePatientDTO): Promise<void> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    this.appointmentsRepository.deleteAppointmentsNotesByPatientId(patient_id);

    Object.assign(patient, {
      name: "deleted_name",
      phone: "deleted_phone",
      email: "deleted_email",
      birth_date: new Date(),
      active: false,
    });

    this.patientsRepository.save(patient);
  }
}

export { DeletePatientLgpdUseCase };
