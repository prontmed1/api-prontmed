import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListPatientsUseCase } from "./ListPatientsUseCase";

class ListPatientsController {
  async handle(request: Request, response: Response): Promise<Response> {
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;

    const { id: doctor_id } = request.doctor;

    const listPatientsUseCase = container.resolve(ListPatientsUseCase);

    const patients = await listPatientsUseCase.execute({
      doctor_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(patients);
  }
}

export { ListPatientsController };
