import { inject, injectable } from "tsyringe";

import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IListPatientsDTO } from "@modules/patients/dtos/IListPatientsDTO";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ListPatientsUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    doctor_id,
    offset,
    limit,
  }: IListPatientsDTO): Promise<Patient[]> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patients = await this.patientsRepository.findByDoctor({
      doctor_id,
      offset,
      limit,
    });

    return patients;
  }
}

export { ListPatientsUseCase };
