import { v4 as uuidV4 } from "uuid";

import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreatePatientUseCase } from "../createPatient/CreatePatientUseCase";
import { ListPatientsUseCase } from "./ListPatientsUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let listPatientsUseCase: ListPatientsUseCase;

let doctorId: string;
let patient: Patient;

describe("List Patients", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    listPatientsUseCase = new ListPatientsUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to list patients", async () => {
    const patients = await listPatientsUseCase.execute({
      doctor_id: patient.doctor_id,
    });

    expect(patients).toEqual([patient]);
  });

  it("should not be able to list patients to a non-existent doctor", async () => {
    await expect(
      listPatientsUseCase.execute({
        doctor_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should be able to list patients with offset and limit", async () => {
    const patientTwo = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test Two",
      phone: "+5548966666666",
      email: "patient2@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test Three",
      phone: "+5548999999999",
      email: "patient3@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const patients = await listPatientsUseCase.execute({
      doctor_id: doctorId,
      offset: 1,
      limit: 1,
    });

    expect(patients).toEqual([patientTwo]);
    expect(patients.length).toBe(1);
  });
});
