import { Request, Response } from "express";
import { container } from "tsyringe";

import { ReadPatientUseCase } from "./ReadPatientUseCase";

class ReadPatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const readPatientUseCase = container.resolve(ReadPatientUseCase);

    const patient = await readPatientUseCase.execute({
      patient_id,
      doctor_id,
    });

    return response.json(patient);
  }
}

export { ReadPatientController };
