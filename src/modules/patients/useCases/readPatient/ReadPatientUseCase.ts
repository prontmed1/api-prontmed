import { inject, injectable } from "tsyringe";

import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IReadPatientDTO } from "@modules/patients/dtos/IReadPatientDTO";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ReadPatientUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({ patient_id, doctor_id }: IReadPatientDTO): Promise<Patient> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
      active: true,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    return patient;
  }
}

export { ReadPatientUseCase };
