import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeletePatientUseCase } from "./DeletePatientUseCase";

class DeletePatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const deletePatientUseCase = container.resolve(DeletePatientUseCase);

    await deletePatientUseCase.execute({
      patient_id,
      doctor_id,
    });

    return response.status(204).send();
  }
}

export { DeletePatientController };
