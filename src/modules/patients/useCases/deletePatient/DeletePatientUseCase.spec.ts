import { v4 as uuidV4 } from "uuid";

import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreatePatientUseCase } from "../createPatient/CreatePatientUseCase";
import { DeletePatientUseCase } from "./DeletePatientUseCase";

let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let patientsRepositoryInMemory: PatientsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let deletePatientUseCase: DeletePatientUseCase;

let doctorId: string;
let patient: Patient;

describe("Delete Patient", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    deletePatientUseCase = new DeletePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to delete patients", async () => {
    expect(
      await deletePatientUseCase.execute({
        doctor_id: doctorId,
        patient_id: patient.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("should not be able to delete patients to a non-existent doctor", async () => {
    await expect(
      deletePatientUseCase.execute({
        doctor_id: uuidV4(),
        patient_id: patient.id,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not be able to delete non-existent patients", async () => {
    await expect(
      deletePatientUseCase.execute({
        doctor_id: doctorId,
        patient_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});
