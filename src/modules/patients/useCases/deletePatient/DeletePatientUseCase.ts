import { inject, injectable } from "tsyringe";

import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IDeletePatientDTO } from "@modules/patients/dtos/IDeletePatientDTO";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class DeletePatientUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({ patient_id, doctor_id }: IDeletePatientDTO): Promise<void> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
      active: true,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    this.patientsRepository.delete(patient_id);
  }
}

export { DeletePatientUseCase };
