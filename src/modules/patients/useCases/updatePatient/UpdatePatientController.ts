import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdatePatientUseCase } from "./UpdatePatientUseCase";

class UpdatePatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, phone, email, birth_date, gender, height, weight } =
      request.body;
    const { patient_id } = request.params;
    const { id: doctor_id } = request.doctor;

    const updatePatientUseCase = container.resolve(UpdatePatientUseCase);

    await updatePatientUseCase.execute({
      patient_id,
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    return response.status(204).send();
  }
}

export { UpdatePatientController };
