import { v4 as uuidV4 } from "uuid";

import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreatePatientUseCase } from "../createPatient/CreatePatientUseCase";
import { UpdatePatientUseCase } from "./UpdatePatientUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;
let updatePatientUseCase: UpdatePatientUseCase;

let doctorId: string;
let patient: Patient;

describe("Update Patient", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );
    updatePatientUseCase = new UpdatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;

    patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to update a patient", async () => {
    const patientUpdated = await updatePatientUseCase.execute({
      patient_id: patient.id,
      doctor_id: patient.doctor_id,
      name: "Patient Test Updated",
      phone: "+5548999669966",
      email: "patientupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const patientFound = await patientsRepositoryInMemory.findByIdAndDoctorId({
      patient_id: patient.id,
      doctor_id: doctorId,
    });

    expect(patientFound.name).toBe(patientUpdated.name);
    expect(patientFound.email).toBe(patientUpdated.email);
  });

  it("should not be able to update a patient if there is another one with the same email", async () => {
    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548966666666",
      email: "patientupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      updatePatientUseCase.execute({
        doctor_id: doctorId,
        patient_id: patient.id,
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patientupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Email already exists"));
  });

  it("should not be able to update a patient if there is another one with the same phone", async () => {
    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548966666666",
      email: "patientupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      updatePatientUseCase.execute({
        doctor_id: doctorId,
        patient_id: patient.id,
        name: "Patient Test",
        phone: "+5548966666666",
        email: "patient@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Phone already exists"));
  });

  it("should be able to update a patient with the same email/phone if it is a different doctor", async () => {
    const doctorTwo = await createDoctorUseCase.execute({
      name: "Doctor Test Two",
      email: "doctor2@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    await createPatientUseCase.execute({
      doctor_id: doctorTwo.id,
      name: "Patient Test",
      phone: "+5548966666666",
      email: "patientupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const patientUpdated = await updatePatientUseCase.execute({
      doctor_id: doctorId,
      patient_id: patient.id,
      name: "Patient Test",
      phone: "+5548966666666",
      email: "patientupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const patientFound = await patientsRepositoryInMemory.findByIdAndDoctorId({
      patient_id: patient.id,
      doctor_id: doctorId,
    });

    expect(patientFound.email).toBe(patientUpdated.email);
  });

  it("should not be able to update a patient to a non-existent doctor", async () => {
    await expect(
      updatePatientUseCase.execute({
        doctor_id: uuidV4(),
        patient_id: patient.id,
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patientupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not be able to update a non-existent patient", async () => {
    await expect(
      updatePatientUseCase.execute({
        doctor_id: doctorId,
        patient_id: uuidV4(),
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patientupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});
