import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("Update Patient Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctors(id, name, email, password, appointment_duration, created_at, updated_at, active)
        values('${id}', 'Doctor Test', 'doctor@example.com', '${password}', 29, 'now()', 'now()', true)`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to update a patient", async () => {
    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "doctor@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patients")
      .send({
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patient@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseList = await request(app)
      .get("/patients")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const patients = responseList.body;

    const response = await request(app)
      .put(`/patients/${patients[0].id}`)
      .send({
        name: "Patient Test Updated",
        phone: "+5548999669966",
        email: "patientupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseRead = await request(app)
      .get(`/patients/${patients[0].id}`)
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(204);
    expect(responseRead.body.email).toEqual("patientupdated@example.com");
    expect(responseRead.body.name).toEqual("Patient Test Updated");
  });

  it("should not be able to update a non-existant patient", async () => {
    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "doctor@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    const response = await request(app)
      .put(`/patients/${uuid()}`)
      .send({
        name: "Patient Test Updated",
        phone: "+5548999669966",
        email: "patientupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });
});
