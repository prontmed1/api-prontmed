import { inject, injectable } from "tsyringe";

import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IUpdatePatientDTO } from "@modules/patients/dtos/IUpdatePatientDTO";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class UpdatePatientUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    patient_id,
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: IUpdatePatientDTO): Promise<Patient> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientsRepository.findByIdAndDoctorId({
      patient_id,
      doctor_id,
      active: true,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    const emailAlreadyExists =
      await this.patientsRepository.findByEmailAndDoctorId({
        email,
        doctor_id,
      });

    if (emailAlreadyExists && emailAlreadyExists.id !== patient_id) {
      throw new AppError("Email already exists");
    }

    const phoneAlreadyExists =
      await this.patientsRepository.findByPhoneAndDoctorId({
        phone,
        doctor_id,
      });

    if (phoneAlreadyExists && phoneAlreadyExists.id !== patient_id) {
      throw new AppError("Phone already exists");
    }

    const newBirthDate = new Date(birth_date).toISOString();

    Object.assign(patient, {
      name,
      phone,
      email,
      birth_date: newBirthDate,
      gender,
      height,
      weight,
    });

    this.patientsRepository.save(patient);

    return patient;
  }
}

export { UpdatePatientUseCase };
