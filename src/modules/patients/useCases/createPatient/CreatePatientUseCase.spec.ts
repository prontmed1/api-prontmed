import { v4 as uuidV4 } from "uuid";

import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { PatientsRepositoryInMemory } from "@modules/patients/repositories/in-memory/PatientsRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreatePatientUseCase } from "./CreatePatientUseCase";

let patientsRepositoryInMemory: PatientsRepositoryInMemory;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let createPatientUseCase: CreatePatientUseCase;

let doctorId: string;

describe("Create Patient", () => {
  beforeEach(async () => {
    patientsRepositoryInMemory = new PatientsRepositoryInMemory();
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    createPatientUseCase = new CreatePatientUseCase(
      patientsRepositoryInMemory,
      doctorsRepositoryInMemory
    );

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;
  });

  it("should be able to create a new patient", async () => {
    const patient = await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    expect(patient).toBeInstanceOf(Patient);
    expect(patient).toHaveProperty("id");
  });

  it("should not be able to create a new patient if there is another one with the same email", async () => {
    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      createPatientUseCase.execute({
        doctor_id: doctorId,
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patient@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Email already exists"));
  });

  it("should not be able to create a new patient if there is another one with the same phone", async () => {
    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      createPatientUseCase.execute({
        doctor_id: doctorId,
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patient2@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Phone already exists"));
  });

  it("should be able to create a new patient with the same email/phone if it is a different doctor", async () => {
    const doctorTwo = await createDoctorUseCase.execute({
      name: "Doctor Test Two",
      email: "doctor2@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    await createPatientUseCase.execute({
      doctor_id: doctorId,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const patient = await createPatientUseCase.execute({
      doctor_id: doctorTwo.id,
      name: "Patient Test",
      phone: "+5548999669966",
      email: "patient@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    expect(patient).toBeInstanceOf(Patient);
    expect(patient).toHaveProperty("id");
  });

  it("should not be able to add a new patient to a non-existent doctor", async () => {
    await expect(
      createPatientUseCase.execute({
        doctor_id: uuidV4(),
        name: "Patient Test",
        phone: "+5548999669966",
        email: "patient@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });
});
