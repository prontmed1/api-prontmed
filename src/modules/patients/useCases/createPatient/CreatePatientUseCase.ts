import { inject, injectable } from "tsyringe";

import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { ICreatePatientDTO } from "@modules/patients/dtos/ICreatePatientDTO";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class CreatePatientUseCase {
  constructor(
    @inject("PatientsRepository")
    private patientsRepository: IPatientsRepository,
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const emailAlreadyExists =
      await this.patientsRepository.findByEmailAndDoctorId({
        email,
        doctor_id,
      });

    if (emailAlreadyExists) {
      throw new AppError("Email already exists");
    }

    const phoneAlreadyExists =
      await this.patientsRepository.findByPhoneAndDoctorId({
        phone,
        doctor_id,
      });

    if (phoneAlreadyExists) {
      throw new AppError("Phone already exists");
    }

    const newBirthDate = new Date(birth_date).toISOString();

    const patient = await this.patientsRepository.create({
      doctor_id,
      name,
      phone,
      email,
      birth_date: newBirthDate,
      gender,
      height,
      weight,
    });

    return patient;
  }
}

export { CreatePatientUseCase };
