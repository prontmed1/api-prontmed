import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from "typeorm";
import { v4 as uuidV4 } from "uuid";

import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";

@Entity("patients")
class Patient {
  @PrimaryColumn()
  id: string;

  @Column()
  doctor_id: string;

  @ManyToOne(() => Doctor)
  @JoinColumn({ name: "doctor_id" })
  doctor: Doctor;

  @Column({ length: 100 })
  name: string;

  @Column({ length: 14 })
  phone: string;

  @Column({ length: 250 })
  email: string;

  @Column("date")
  birth_date: Date;

  @Column({ length: 1 })
  gender: string;

  @Column("int")
  height: number;

  @Column("numeric", { precision: 4, scale: 1 })
  weight: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  active: boolean;

  constructor() {
    if (!this.id) {
      this.id = uuidV4();
    }
  }
}

export { Patient };
