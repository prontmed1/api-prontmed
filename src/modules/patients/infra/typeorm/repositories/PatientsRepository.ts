import { getRepository, Repository } from "typeorm";

import { ICreatePatientDTO } from "@modules/patients/dtos/ICreatePatientDTO";
import { IFindByDoctorDTO } from "@modules/patients/dtos/IFindByDoctorDTO";
import { IFindByEmailDTO } from "@modules/patients/dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "@modules/patients/dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "@modules/patients/dtos/IFindByPhoneDTO";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";

import { Patient } from "../entities/Patient";

class PatientsRepository implements IPatientsRepository {
  private repository: Repository<Patient>;

  constructor() {
    this.repository = getRepository(Patient);
  }

  async create({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const patient = this.repository.create({
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    await this.repository.save(patient);

    return patient;
  }

  async findByEmailAndDoctorId({
    email,
    doctor_id,
  }: IFindByEmailDTO): Promise<Patient> {
    const patient = await this.repository.findOne({
      email,
      doctor_id,
      active: true,
    });
    return patient;
  }

  async findByPhoneAndDoctorId({
    phone,
    doctor_id,
  }: IFindByPhoneDTO): Promise<Patient> {
    const patient = await this.repository.findOne({
      phone,
      doctor_id,
      active: true,
    });
    return patient;
  }

  async findByDoctor({
    doctor_id,
    offset,
    limit,
  }: IFindByDoctorDTO): Promise<Patient[]> {
    const findOptions = {
      where: { doctor_id, active: true },
    };

    if (offset) {
      Object.assign(findOptions, { skip: offset });
    }

    if (limit) {
      Object.assign(findOptions, { take: limit });
    }

    const patients = await this.repository.find(findOptions);

    return patients;
  }

  async findByIdAndDoctorId({
    patient_id,
    doctor_id,
    active,
  }: IFindByIdDTO): Promise<Patient> {
    const where = {
      id: patient_id,
      doctor_id,
    };

    if (active === true) {
      Object.assign(where, { active: true });
    }

    const patient = await this.repository.findOne(where);

    return patient;
  }

  async save(patient: Patient): Promise<void> {
    this.repository.save(patient);
  }

  async delete(patient_id: string): Promise<void> {
    this.repository.update({ id: patient_id }, { active: false });
  }
}

export { PatientsRepository };
