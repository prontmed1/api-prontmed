interface IDeletePatientDTO {
  patient_id: string;
  doctor_id: string;
}

export { IDeletePatientDTO };
