interface IFindByPhoneDTO {
  phone: string;
  doctor_id: string;
}

export { IFindByPhoneDTO };
