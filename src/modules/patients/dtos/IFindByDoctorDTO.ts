interface IFindByDoctorDTO {
  doctor_id: string;
  offset?: number;
  limit?: number;
}

export { IFindByDoctorDTO };
