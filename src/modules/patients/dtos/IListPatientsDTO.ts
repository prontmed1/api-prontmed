interface IListPatientsDTO {
  doctor_id: string;
  offset?: number;
  limit?: number;
}

export { IListPatientsDTO };
