interface IReadPatientDTO {
  patient_id: string;
  doctor_id: string;
}

export { IReadPatientDTO };
