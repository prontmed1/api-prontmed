interface IUpdatePatientDTO {
  patient_id: string;
  doctor_id: string;
  name: string;
  phone: string;
  email: string;
  birth_date: string;
  gender: string;
  height: number;
  weight: number;
}

export { IUpdatePatientDTO };
