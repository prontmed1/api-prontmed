interface IFindByIdDTO {
  patient_id: string;
  doctor_id: string;
  active?: boolean;
}

export { IFindByIdDTO };
