interface IFindByEmailDTO {
  email: string;
  doctor_id: string;
}

export { IFindByEmailDTO };
