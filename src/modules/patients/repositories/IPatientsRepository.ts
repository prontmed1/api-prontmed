import { ICreatePatientDTO } from "../dtos/ICreatePatientDTO";
import { IFindByDoctorDTO } from "../dtos/IFindByDoctorDTO";
import { IFindByEmailDTO } from "../dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "../dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "../dtos/IFindByPhoneDTO";
import { Patient } from "../infra/typeorm/entities/Patient";

interface IPatientsRepository {
  create(data: ICreatePatientDTO): Promise<Patient>;
  findByDoctor(data: IFindByDoctorDTO): Promise<Patient[]>;
  findByIdAndDoctorId(data: IFindByIdDTO): Promise<Patient>;
  findByEmailAndDoctorId(data: IFindByEmailDTO): Promise<Patient>;
  findByPhoneAndDoctorId(data: IFindByPhoneDTO): Promise<Patient>;
  save(patient: Patient): Promise<void>;
  delete(patient_id: string): Promise<void>;
}

export { IPatientsRepository };
