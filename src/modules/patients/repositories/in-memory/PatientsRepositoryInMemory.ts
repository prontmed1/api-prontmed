import { ICreatePatientDTO } from "@modules/patients/dtos/ICreatePatientDTO";
import { IFindByDoctorDTO } from "@modules/patients/dtos/IFindByDoctorDTO";
import { IFindByEmailDTO } from "@modules/patients/dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "@modules/patients/dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "@modules/patients/dtos/IFindByPhoneDTO";
import { Patient } from "@modules/patients/infra/typeorm/entities/Patient";

import { IPatientsRepository } from "../IPatientsRepository";

class PatientsRepositoryInMemory implements IPatientsRepository {
  patients: Patient[] = [];

  async create({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const patient = new Patient();

    Object.assign(patient, {
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    this.patients.push(patient);

    return patient;
  }

  async findByDoctor({
    doctor_id,
    offset,
    limit,
  }: IFindByDoctorDTO): Promise<Patient[]> {
    const patients = this.patients.filter(
      (patientInMemory) => patientInMemory.doctor_id === doctor_id
    );

    if (offset && limit) {
      return patients.slice(offset, limit + 1);
    }

    return patients;
  }

  async findByIdAndDoctorId({
    doctor_id,
    patient_id,
  }: IFindByIdDTO): Promise<Patient> {
    const patient = this.patients.find(
      (patientInMemory) =>
        patientInMemory.doctor_id === doctor_id &&
        patientInMemory.id === patient_id
    );

    return patient;
  }

  async findByEmailAndDoctorId({
    doctor_id,
    email,
  }: IFindByEmailDTO): Promise<Patient> {
    const patient = this.patients.find(
      (patientInMemory) =>
        patientInMemory.doctor_id === doctor_id &&
        patientInMemory.email === email
    );

    return patient;
  }

  async findByPhoneAndDoctorId({
    doctor_id,
    phone,
  }: IFindByPhoneDTO): Promise<Patient> {
    const patient = this.patients.find(
      (patientInMemory) =>
        patientInMemory.doctor_id === doctor_id &&
        patientInMemory.phone === phone
    );

    return patient;
  }

  async save(patient: Patient): Promise<void> {
    const findIndex = this.patients.findIndex(
      (patientInMemory) => patientInMemory.id === patient.id
    );

    this.patients[findIndex] = patient;
  }

  async delete(patient_id: string): Promise<void> {
    const patient = this.patients.find(
      (patientInMemory) => patientInMemory.id === patient_id
    );

    this.patients.splice(this.patients.indexOf(patient));
  }
}

export { PatientsRepositoryInMemory };
