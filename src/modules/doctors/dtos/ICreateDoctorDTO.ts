interface ICreateDoctorDTO {
  name: string;
  email: string;
  password: string;
  appointment_duration: number;
}

export { ICreateDoctorDTO };
