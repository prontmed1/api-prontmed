interface IFindByDoctorDTO {
  doctor_id: string;
  refresh_token: string;
}

export { IFindByDoctorDTO };
