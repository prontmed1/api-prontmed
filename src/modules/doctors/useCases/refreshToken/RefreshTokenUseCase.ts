import { verify, sign } from "jsonwebtoken";
import { inject, injectable } from "tsyringe";

import auth from "@config/auth";
import { IDoctorsTokensRepository } from "@modules/doctors/repositories/IDoctorsTokensRepository";
import { IDateProvider } from "@shared/container/providers/DateProvider/IDateProvider";
import { AppError } from "@shared/errors/AppError";

interface IPayload {
  sub: string;
  email: string;
}

interface ITokenResponse {
  access_token: string;
  refresh_token: string;
}

@injectable()
class RefreshTokenUseCase {
  constructor(
    @inject("DoctorsTokensRepository")
    private doctorsTokensRepository: IDoctorsTokensRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute(refresh_token: string): Promise<ITokenResponse> {
    const { email, sub: doctor_id } = verify(
      refresh_token,
      auth.secret_refresh_token
    ) as IPayload;

    const doctorToken =
      await this.doctorsTokensRepository.findByDoctorIdAndRefreshToken({
        doctor_id,
        refresh_token,
      });

    if (!doctorToken) {
      throw new AppError("Refresh Token does not exists!");
    }

    await this.doctorsTokensRepository.deleteById(doctorToken.id);

    const refreshToken = sign({ email }, auth.secret_refresh_token, {
      subject: doctor_id,
      expiresIn: auth.expires_in_refresh_token,
    });

    const expiresDate = this.dateProvider.addDays(
      auth.expires_refresh_token_days
    );

    await this.doctorsTokensRepository.create({
      expires_date: expiresDate,
      refresh_token: refreshToken,
      doctor_id,
    });

    const newAccessToken = sign({}, auth.secret_token, {
      subject: doctor_id,
      expiresIn: auth.expires_in_token,
    });

    return {
      refresh_token: refreshToken,
      access_token: newAccessToken,
    };
  }
}

export { RefreshTokenUseCase };
