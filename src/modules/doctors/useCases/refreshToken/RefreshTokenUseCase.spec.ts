import { JsonWebTokenError, sign } from "jsonwebtoken";

import auth from "@config/auth";
import { ICreateDoctorDTO } from "@modules/doctors/dtos/ICreateDoctorDTO";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { DoctorsTokensRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsTokensRepositoryInMemory";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { AuthenticateDoctorUseCase } from "../authenticateDoctor/AuthenticateDoctorUseCase";
import { CreateDoctorUseCase } from "../createDoctor/CreateDoctorUseCase";
import { RefreshTokenUseCase } from "./RefreshTokenUseCase";

let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let doctorsTokensRepositoryInMemory: DoctorsTokensRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createDoctorUseCase: CreateDoctorUseCase;
let authenticateDoctorUseCase: AuthenticateDoctorUseCase;
let refreshTokenUseCase: RefreshTokenUseCase;

describe("Refresh Token", () => {
  beforeEach(() => {
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    doctorsTokensRepositoryInMemory = new DoctorsTokensRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    authenticateDoctorUseCase = new AuthenticateDoctorUseCase(
      doctorsRepositoryInMemory,
      doctorsTokensRepositoryInMemory,
      dateProvider
    );
    refreshTokenUseCase = new RefreshTokenUseCase(
      doctorsTokensRepositoryInMemory,
      dateProvider
    );
  });

  it("should be able to generate a new token", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    };

    await createDoctorUseCase.execute(doctor);

    const authentication = await authenticateDoctorUseCase.execute({
      email: doctor.email,
      password: doctor.password,
    });

    const refreshToken = await refreshTokenUseCase.execute(
      authentication.refresh_token
    );

    expect(refreshToken).toHaveProperty("access_token");
    expect(refreshToken).toHaveProperty("refresh_token");
  });

  it("should not be able to generate a refresh token for a non-existent token", async () => {
    const invalidRefreshToken = sign(
      { email: "false@example.com" },
      auth.secret_refresh_token
    );

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toEqual(new AppError("Refresh Token does not exists!"));
  });

  it("should not be able to generate a refresh token for a invalid signature", async () => {
    const invalidRefreshToken = sign(
      { email: "false@example.com" },
      "invalidSignature"
    );

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });

  it("should not be able to generate a refresh token for a invalid jwt", async () => {
    const invalidRefreshToken = "invalidJwt";

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });
});
