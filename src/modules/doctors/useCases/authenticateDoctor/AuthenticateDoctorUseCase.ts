import { compare } from "bcrypt";
import { sign } from "jsonwebtoken";
import { inject, injectable } from "tsyringe";

import auth from "@config/auth";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IDoctorsTokensRepository } from "@modules/doctors/repositories/IDoctorsTokensRepository";
import { IDateProvider } from "@shared/container/providers/DateProvider/IDateProvider";
import { AppError } from "@shared/errors/AppError";

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  doctor: {
    name: string;
    email: string;
  };
  access_token: string;
  refresh_token: string;
}

@injectable()
class AuthenticateDoctorUseCase {
  constructor(
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository,
    @inject("DoctorsTokensRepository")
    private doctorsTokensRepository: IDoctorsTokensRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute({ email, password }: IRequest): Promise<IResponse> {
    const doctor = await this.doctorsRepository.findByEmail(email);

    const {
      expires_in_token,
      secret_refresh_token,
      secret_token,
      expires_in_refresh_token,
      expires_refresh_token_days,
    } = auth;

    if (!doctor) {
      throw new AppError("Email or password incorrect!");
    }

    const passwordMatch = await compare(password, doctor.password);

    if (!passwordMatch) {
      throw new AppError("Email or password incorrect!");
    }

    const accessToken = sign({}, secret_token, {
      subject: doctor.id,
      expiresIn: expires_in_token,
    });

    const refreshToken = sign({ email }, secret_refresh_token, {
      subject: doctor.id,
      expiresIn: expires_in_refresh_token,
    });

    const refreshTokenExpiresDate = this.dateProvider.addDays(
      expires_refresh_token_days
    );

    await this.doctorsTokensRepository.create({
      doctor_id: doctor.id,
      refresh_token: refreshToken,
      expires_date: refreshTokenExpiresDate,
    });

    const doctorAndTokens: IResponse = {
      doctor: {
        name: doctor.name,
        email: doctor.email,
      },
      access_token: accessToken,
      refresh_token: refreshToken,
    };

    return doctorAndTokens;
  }
}

export { AuthenticateDoctorUseCase };
