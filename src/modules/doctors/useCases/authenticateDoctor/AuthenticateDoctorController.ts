import { Request, Response } from "express";
import { container } from "tsyringe";

import { AuthenticateDoctorUseCase } from "./AuthenticateDoctorUseCase";

class AuthenticateDoctorController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body;

    const authenticateDoctorUseCase = container.resolve(
      AuthenticateDoctorUseCase
    );

    const doctorAndTokens = await authenticateDoctorUseCase.execute({
      email,
      password,
    });

    return response.json(doctorAndTokens);
  }
}

export { AuthenticateDoctorController };
