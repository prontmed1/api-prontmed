import { ICreateDoctorDTO } from "@modules/doctors/dtos/ICreateDoctorDTO";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { DoctorsTokensRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsTokensRepositoryInMemory";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { CreateDoctorUseCase } from "../createDoctor/CreateDoctorUseCase";
import { AuthenticateDoctorUseCase } from "./AuthenticateDoctorUseCase";

let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;
let doctorsTokensRepositoryInMemory: DoctorsTokensRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createDoctorUseCase: CreateDoctorUseCase;
let authenticateDoctorUseCase: AuthenticateDoctorUseCase;

describe("Authenticate Doctor", () => {
  beforeEach(() => {
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    doctorsTokensRepositoryInMemory = new DoctorsTokensRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    authenticateDoctorUseCase = new AuthenticateDoctorUseCase(
      doctorsRepositoryInMemory,
      doctorsTokensRepositoryInMemory,
      dateProvider
    );
  });

  it("should be able to authenticate a doctor", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    };

    await createDoctorUseCase.execute(doctor);

    const authentication = await authenticateDoctorUseCase.execute({
      email: doctor.email,
      password: doctor.password,
    });

    expect(authentication).toHaveProperty("access_token");
    expect(authentication).toHaveProperty("refresh_token");
    expect(authentication.doctor.name).toEqual(doctor.name);
    expect(authentication.doctor.email).toEqual(doctor.email);
  });

  it("should not be able to authenticate a non-existent doctor", async () => {
    await expect(
      authenticateDoctorUseCase.execute({
        email: "false@example.com",
        password: "123456",
      })
    ).rejects.toEqual(new AppError("Email or password incorrect!"));
  });

  it("should not be able to authenticate with incorrect password", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    };

    await createDoctorUseCase.execute(doctor);

    await expect(
      authenticateDoctorUseCase.execute({
        email: doctor.email,
        password: "654321",
      })
    ).rejects.toEqual(new AppError("Email or password incorrect!"));
  });
});
