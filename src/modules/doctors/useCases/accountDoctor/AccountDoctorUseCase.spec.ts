import { v4 as uuidV4 } from "uuid";

import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { CreateDoctorUseCase } from "@modules/doctors/useCases/createDoctor/CreateDoctorUseCase";
import { AppError } from "@shared/errors/AppError";

import { AccountDoctorUseCase } from "./AccountDoctorUseCase";

let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;

let createDoctorUseCase: CreateDoctorUseCase;
let accountDoctorUseCase: AccountDoctorUseCase;

let doctorId: string;

describe("Account Doctor", () => {
  beforeEach(async () => {
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();

    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
    accountDoctorUseCase = new AccountDoctorUseCase(doctorsRepositoryInMemory);

    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    doctorId = doctor.id;
  });

  it("should be able to read the doctor's account", async () => {
    const doctorAccount = await accountDoctorUseCase.execute(doctorId);

    expect(doctorAccount).toBeInstanceOf(Doctor);
    expect(doctorAccount).toHaveProperty("id");
  });

  it("should not be able to read a non-existent doctor's account", async () => {
    await expect(accountDoctorUseCase.execute(uuidV4())).rejects.toEqual(
      new AppError("Doctor not found")
    );
  });
});
