import { Request, Response } from "express";
import { container } from "tsyringe";

import { AccountDoctorUseCase } from "./AccountDoctorUseCase";

class AccountDoctorController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: doctor_id } = request.doctor;

    const accountDoctorUseCase = container.resolve(AccountDoctorUseCase);

    const doctor = await accountDoctorUseCase.execute(doctor_id);

    return response.json(doctor);
  }
}

export { AccountDoctorController };
