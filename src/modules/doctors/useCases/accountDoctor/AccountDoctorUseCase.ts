import { inject, injectable } from "tsyringe";

import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class AccountDoctorUseCase {
  constructor(
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute(doctor_id: string): Promise<Doctor> {
    const doctor = await this.doctorsRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    return doctor;
  }
}

export { AccountDoctorUseCase };
