import { hash } from "bcrypt";
import { inject, injectable } from "tsyringe";

import { ICreateDoctorDTO } from "@modules/doctors/dtos/ICreateDoctorDTO";
import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class CreateDoctorUseCase {
  constructor(
    @inject("DoctorsRepository")
    private doctorsRepository: IDoctorsRepository
  ) {}

  async execute({
    name,
    email,
    password,
    appointment_duration,
  }: ICreateDoctorDTO): Promise<Doctor> {
    const doctorAlreadyExists = await this.doctorsRepository.findByEmail(email);

    if (doctorAlreadyExists) {
      throw new AppError("Doctor already exists");
    }

    const passwordHash = await hash(password, 8);

    const doctor = await this.doctorsRepository.create({
      name,
      email,
      password: passwordHash,
      appointment_duration,
    });

    return doctor;
  }
}

export { CreateDoctorUseCase };
