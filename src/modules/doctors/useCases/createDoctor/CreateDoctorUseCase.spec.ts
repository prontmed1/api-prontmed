import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";
import { DoctorsRepositoryInMemory } from "@modules/doctors/repositories/in-memory/DoctorsRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreateDoctorUseCase } from "./CreateDoctorUseCase";

let createDoctorUseCase: CreateDoctorUseCase;
let doctorsRepositoryInMemory: DoctorsRepositoryInMemory;

describe("Create Doctor", () => {
  beforeEach(() => {
    doctorsRepositoryInMemory = new DoctorsRepositoryInMemory();
    createDoctorUseCase = new CreateDoctorUseCase(doctorsRepositoryInMemory);
  });

  it("should be able to create a new doctor", async () => {
    const doctor = await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    expect(doctor).toBeInstanceOf(Doctor);
    expect(doctor).toHaveProperty("id");
  });

  it("should not be able to create a new doctor if there is another one with the same email", async () => {
    await createDoctorUseCase.execute({
      name: "Doctor Test",
      email: "doctor@example.com",
      password: "123456",
      appointment_duration: 29,
    });

    await expect(
      createDoctorUseCase.execute({
        name: "Doctor Test",
        email: "doctor@example.com",
        password: "123456",
        appointment_duration: 29,
      })
    ).rejects.toEqual(new AppError("Doctor already exists"));
  });
});
