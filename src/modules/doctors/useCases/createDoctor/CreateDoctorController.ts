import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateDoctorUseCase } from "./CreateDoctorUseCase";

class CreateDoctorController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, email, password, appointment_duration } = request.body;

    const createDoctorUseCase = container.resolve(CreateDoctorUseCase);

    await createDoctorUseCase.execute({
      name,
      email,
      password,
      appointment_duration,
    });

    return response.status(201).send();
  }
}

export { CreateDoctorController };
