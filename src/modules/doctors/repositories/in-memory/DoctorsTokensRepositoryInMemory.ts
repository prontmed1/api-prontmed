import { ICreateDoctorTokenDTO } from "@modules/doctors/dtos/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "@modules/doctors/dtos/IFindByDoctorDTO";
import { DoctorToken } from "@modules/doctors/infra/typeorm/entities/DoctorToken";

import { IDoctorsTokensRepository } from "../IDoctorsTokensRepository";

class DoctorsTokensRepositoryInMemory implements IDoctorsTokensRepository {
  doctorsTokens: DoctorToken[] = [];

  async create({
    doctor_id,
    expires_date,
    refresh_token,
  }: ICreateDoctorTokenDTO): Promise<DoctorToken> {
    const doctorToken = new DoctorToken();

    Object.assign(doctorToken, {
      doctor_id,
      expires_date,
      refresh_token,
    });

    this.doctorsTokens.push(doctorToken);

    return doctorToken;
  }

  async findByDoctorIdAndRefreshToken({
    doctor_id,
    refresh_token,
  }: IFindByDoctorDTO): Promise<DoctorToken> {
    const token = this.doctorsTokens.find(
      (tokenInMemory) =>
        tokenInMemory.doctor_id === doctor_id &&
        tokenInMemory.refresh_token === refresh_token
    );

    return token;
  }

  async deleteById(id: string): Promise<void> {
    const token = this.doctorsTokens.find(
      (tokenInMemory) => tokenInMemory.id === id
    );

    this.doctorsTokens.splice(this.doctorsTokens.indexOf(token));
  }
}

export { DoctorsTokensRepositoryInMemory };
