import { ICreateDoctorDTO } from "@modules/doctors/dtos/ICreateDoctorDTO";
import { Doctor } from "@modules/doctors/infra/typeorm/entities/Doctor";

import { IDoctorsRepository } from "../IDoctorsRepository";

class DoctorsRepositoryInMemory implements IDoctorsRepository {
  doctors: Doctor[] = [];

  async create({
    name,
    email,
    password,
    appointment_duration,
  }: ICreateDoctorDTO): Promise<Doctor> {
    const doctor = new Doctor();

    Object.assign(doctor, {
      name,
      email,
      password,
      appointment_duration,
    });

    this.doctors.push(doctor);

    return doctor;
  }

  async findByEmail(email: string): Promise<Doctor> {
    const doctor = this.doctors.find(
      (doctorInMemory) => doctorInMemory.email === email
    );

    return doctor;
  }

  async findById(id: string): Promise<Doctor> {
    const doctor = this.doctors.find(
      (doctorInMemory) => doctorInMemory.id === id
    );

    return doctor;
  }
}

export { DoctorsRepositoryInMemory };
