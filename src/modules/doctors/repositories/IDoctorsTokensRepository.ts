import { ICreateDoctorTokenDTO } from "../dtos/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "../dtos/IFindByDoctorDTO";
import { DoctorToken } from "../infra/typeorm/entities/DoctorToken";

interface IDoctorsTokensRepository {
  create(data: ICreateDoctorTokenDTO): Promise<DoctorToken>;
  findByDoctorIdAndRefreshToken(data: IFindByDoctorDTO): Promise<DoctorToken>;
  deleteById(id: string): Promise<void>;
}

export { IDoctorsTokensRepository };
