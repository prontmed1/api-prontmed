import { ICreateDoctorDTO } from "../dtos/ICreateDoctorDTO";
import { Doctor } from "../infra/typeorm/entities/Doctor";

interface IDoctorsRepository {
  create(data: ICreateDoctorDTO): Promise<Doctor>;
  findByEmail(email: string): Promise<Doctor>;
  findById(id: string): Promise<Doctor>;
}

export { IDoctorsRepository };
