import { getRepository, Repository } from "typeorm";

import { ICreateDoctorDTO } from "@modules/doctors/dtos/ICreateDoctorDTO";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";

import { Doctor } from "../entities/Doctor";

class DoctorsRepository implements IDoctorsRepository {
  private repository: Repository<Doctor>;

  constructor() {
    this.repository = getRepository(Doctor);
  }

  async create({
    name,
    email,
    password,
    appointment_duration,
  }: ICreateDoctorDTO): Promise<Doctor> {
    const doctor = this.repository.create({
      name,
      email,
      password,
      appointment_duration,
    });

    await this.repository.save(doctor);

    return doctor;
  }

  async findByEmail(email: string): Promise<Doctor> {
    const doctor = await this.repository.findOne({ email, active: true });
    return doctor;
  }

  async findById(id: string): Promise<Doctor> {
    const doctor = await this.repository.findOne(id, {
      select: [
        "id",
        "name",
        "email",
        "appointment_duration",
        "created_at",
        "updated_at",
      ],
      where: { active: true },
    });
    return doctor;
  }
}

export { DoctorsRepository };
