import { getRepository, Repository } from "typeorm";

import { ICreateDoctorTokenDTO } from "@modules/doctors/dtos/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "@modules/doctors/dtos/IFindByDoctorDTO";
import { IDoctorsTokensRepository } from "@modules/doctors/repositories/IDoctorsTokensRepository";

import { DoctorToken } from "../entities/DoctorToken";

class DoctorsTokensRepository implements IDoctorsTokensRepository {
  private repository: Repository<DoctorToken>;

  constructor() {
    this.repository = getRepository(DoctorToken);
  }

  async create({
    doctor_id,
    expires_date,
    refresh_token,
  }: ICreateDoctorTokenDTO): Promise<DoctorToken> {
    const doctorToken = this.repository.create({
      doctor_id,
      expires_date,
      refresh_token,
    });

    await this.repository.save(doctorToken);

    return doctorToken;
  }

  async findByDoctorIdAndRefreshToken({
    doctor_id,
    refresh_token,
  }: IFindByDoctorDTO): Promise<DoctorToken> {
    const doctorToken = await this.repository.findOne({
      doctor_id,
      refresh_token,
    });

    return doctorToken;
  }

  async deleteById(id: string): Promise<void> {
    await this.repository.delete(id);
  }
}

export { DoctorsTokensRepository };
