import { container } from "tsyringe";

import "@shared/container/providers";

import { AppointmentsRepository } from "@modules/appointments/infra/typeorm/repositories/AppointmentsRepository";
import { IAppointmentsRepository } from "@modules/appointments/repositories/IAppointmentsRepository";
import { DoctorsRepository } from "@modules/doctors/infra/typeorm/repositories/DoctorsRepository";
import { DoctorsTokensRepository } from "@modules/doctors/infra/typeorm/repositories/DoctorsTokensRepository";
import { IDoctorsRepository } from "@modules/doctors/repositories/IDoctorsRepository";
import { IDoctorsTokensRepository } from "@modules/doctors/repositories/IDoctorsTokensRepository";
import { PatientsRepository } from "@modules/patients/infra/typeorm/repositories/PatientsRepository";
import { IPatientsRepository } from "@modules/patients/repositories/IPatientsRepository";

container.registerSingleton<IDoctorsRepository>(
  "DoctorsRepository",
  DoctorsRepository
);

container.registerSingleton<IDoctorsTokensRepository>(
  "DoctorsTokensRepository",
  DoctorsTokensRepository
);

container.registerSingleton<IPatientsRepository>(
  "PatientsRepository",
  PatientsRepository
);

container.registerSingleton<IAppointmentsRepository>(
  "AppointmentsRepository",
  AppointmentsRepository
);
