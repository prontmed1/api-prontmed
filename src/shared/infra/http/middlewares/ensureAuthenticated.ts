import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

import auth from "@config/auth";
import { AppError } from "@shared/errors/AppError";

interface IPayload {
  sub: string;
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export async function ensureAuthenticated(
  request: Request,
  response: Response,
  next: NextFunction
) {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError("Token missing", 401);
  }

  const [, accessToken] = authHeader.split(" ");

  try {
    const { sub: doctor_id } = verify(
      accessToken,
      auth.secret_token
    ) as IPayload;

    request.doctor = {
      id: doctor_id,
    };

    next();
  } catch {
    throw new AppError("Invalid token!", 401);
  }
}
