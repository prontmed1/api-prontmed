import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { AuthenticateDoctorController } from "@modules/doctors/useCases/authenticateDoctor/AuthenticateDoctorController";
import { RefreshTokenController } from "@modules/doctors/useCases/refreshToken/RefreshTokenController";

const authenticateRoutes = Router();

const authenticateDoctorController = new AuthenticateDoctorController();
const refreshTokenController = new RefreshTokenController();

authenticateRoutes.post(
  "/sessions",
  celebrate({
    [Segments.BODY]: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  }),
  authenticateDoctorController.handle
);

authenticateRoutes.post(
  "/refresh-token",
  celebrate({
    [Segments.BODY]: {
      refresh_token: Joi.string().required(),
    },
  }),
  refreshTokenController.handle
);

export { authenticateRoutes };
