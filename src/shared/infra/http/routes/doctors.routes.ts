import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { AccountDoctorController } from "@modules/doctors/useCases/accountDoctor/AccountDoctorController";
import { CreateDoctorController } from "@modules/doctors/useCases/createDoctor/CreateDoctorController";

import { ensureAuthenticated } from "../middlewares/ensureAuthenticated";

const doctorsRoutes = Router();

const createDoctorController = new CreateDoctorController();
const accountDoctorController = new AccountDoctorController();

doctorsRoutes.post(
  "/",
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      email: Joi.string().email().required().max(250),
      password: Joi.string().required().min(6),
      appointment_duration: Joi.number()
        .integer()
        .positive()
        .required()
        .max(1440),
    },
  }),
  createDoctorController.handle
);

doctorsRoutes.get(
  "/account",
  ensureAuthenticated,
  accountDoctorController.handle
);

export { doctorsRoutes };
