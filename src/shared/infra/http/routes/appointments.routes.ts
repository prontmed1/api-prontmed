import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { CreateAppointmentController } from "@modules/appointments/useCases/createAppointment/CreateAppointmentController";
import { DeleteAppointmentController } from "@modules/appointments/useCases/deleteAppointment/DeleteAppointmentController";
import { ListAppointmentsController } from "@modules/appointments/useCases/listAppointments/ListAppointmentsController";
import { ListAppointmentsNotesController } from "@modules/appointments/useCases/listAppointmentsNotes/ListAppointmentsNotesController";
import { ReadAppointmentController } from "@modules/appointments/useCases/readAppointment/ReadAppointmentController";
import { ReadAppointmentNoteController } from "@modules/appointments/useCases/readAppointmentNote/ReadAppointmentNoteController";
import { UpdateAppointmentController } from "@modules/appointments/useCases/updateAppointment/UpdateAppointmentController";
import { UpdateAppointmentNotesController } from "@modules/appointments/useCases/updateAppointmentNotes/UpdateAppointmentNotesController";
import { ensureAuthenticated } from "@shared/infra/http/middlewares/ensureAuthenticated";

const appointmentsRoutes = Router();

const createAppointmentController = new CreateAppointmentController();
const listAppointmentsController = new ListAppointmentsController();
const updateAppointmentController = new UpdateAppointmentController();
const deleteAppointmentController = new DeleteAppointmentController();
const updateAppointmentNotesController = new UpdateAppointmentNotesController();
const listAppointmentsNotesController = new ListAppointmentsNotesController();
const readAppointmentController = new ReadAppointmentController();
const readAppointmentNoteController = new ReadAppointmentNoteController();

appointmentsRoutes.post(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      patient_id: Joi.string().uuid().required(),
      date: Joi.date().iso().required(),
      notes: Joi.string().allow("", null),
    },
  }),
  createAppointmentController.handle
);

appointmentsRoutes.get(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.QUERY]: {
      patient_id: Joi.string().uuid(),
      start_date: Joi.date().iso().required(),
      end_date: Joi.date().iso().required(),
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listAppointmentsController.handle
);

appointmentsRoutes.put(
  "/:appointment_id",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      patient_id: Joi.string().uuid().required(),
      date: Joi.date().iso().required(),
      notes: Joi.string().allow("", null),
    },
    [Segments.PARAMS]: {
      appointment_id: Joi.string().uuid().required(),
    },
  }),
  updateAppointmentController.handle
);

appointmentsRoutes.delete(
  "/:appointment_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      appointment_id: Joi.string().uuid().required(),
    },
  }),
  deleteAppointmentController.handle
);

appointmentsRoutes.patch(
  "/:appointment_id/notes",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      notes: Joi.string().required().allow("", null),
    },
    [Segments.PARAMS]: {
      appointment_id: Joi.string().uuid().required(),
    },
  }),
  updateAppointmentNotesController.handle
);

appointmentsRoutes.get(
  "/notes",
  ensureAuthenticated,
  celebrate({
    [Segments.QUERY]: {
      patient_id: Joi.string().uuid().required(),
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listAppointmentsNotesController.handle
);

appointmentsRoutes.get(
  "/:appointment_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      appointment_id: Joi.string().uuid().required(),
    },
  }),
  readAppointmentController.handle
);

appointmentsRoutes.get(
  "/:appointment_id/notes",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      appointment_id: Joi.string().uuid().required(),
    },
  }),
  readAppointmentNoteController.handle
);

export { appointmentsRoutes };
