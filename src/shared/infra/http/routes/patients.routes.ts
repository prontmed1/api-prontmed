import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { CreatePatientController } from "@modules/patients/useCases/createPatient/CreatePatientController";
import { DeletePatientController } from "@modules/patients/useCases/deletePatient/DeletePatientController";
import { DeletePatientLgpdController } from "@modules/patients/useCases/deletePatientLgpd/DeletePatientLgpdController";
import { ListPatientsController } from "@modules/patients/useCases/listPatients/ListPatientsController";
import { ReadPatientController } from "@modules/patients/useCases/readPatient/ReadPatientController";
import { UpdatePatientController } from "@modules/patients/useCases/updatePatient/UpdatePatientController";
import { ensureAuthenticated } from "@shared/infra/http/middlewares/ensureAuthenticated";

const patientsRoutes = Router();

const createPatientController = new CreatePatientController();
const updatePatientController = new UpdatePatientController();
const deletePatientController = new DeletePatientController();
const deletePatientLgpdController = new DeletePatientLgpdController();
const listPatientsController = new ListPatientsController();
const readPatientController = new ReadPatientController();

patientsRoutes.post(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      phone: Joi.string().required().length(14),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F"),
      height: Joi.number().integer().positive().required().max(300),
      weight: Joi.number().positive().required().max(400),
    },
  }),
  createPatientController.handle
);

patientsRoutes.put(
  "/:patient_id",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      phone: Joi.string().required().length(14),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F"),
      height: Joi.number().integer().positive().required().max(300),
      weight: Joi.number().positive().required().max(400),
    },
    [Segments.PARAMS]: {
      patient_id: Joi.string().uuid().required(),
    },
  }),
  updatePatientController.handle
);

patientsRoutes.get(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.QUERY]: {
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listPatientsController.handle
);

patientsRoutes.delete(
  "/:patient_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      patient_id: Joi.string().uuid().required(),
    },
  }),
  deletePatientController.handle
);

patientsRoutes.delete(
  "/:patient_id/lgpd",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      patient_id: Joi.string().uuid().required(),
    },
  }),
  deletePatientLgpdController.handle
);

patientsRoutes.get(
  "/:patient_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      patient_id: Joi.string().uuid().required(),
    },
  }),
  readPatientController.handle
);

export { patientsRoutes };
