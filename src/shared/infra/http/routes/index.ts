import { Router } from "express";

import { appointmentsRoutes } from "./appointments.routes";
import { authenticateRoutes } from "./authenticate.routes";
import { doctorsRoutes } from "./doctors.routes";
import { patientsRoutes } from "./patients.routes";

const router = Router();

router.get("/", (request, response) => {
  response.send(
    `<h1 style="font-family: Arial;">Bem-vindo a API ProntMed!</h1>
    <p style="font-family: Arial; font-size: 16px;">Para acessar a documentação da API <a href="/api-docs">clique aqui</a>.</p>`
  );
});

router.use("/doctors", doctorsRoutes);
router.use("/auth", authenticateRoutes);
router.use("/patients", patientsRoutes);
router.use("/appointments", appointmentsRoutes);

export { router };
