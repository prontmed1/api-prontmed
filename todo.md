# Rest API ProntMed to-do

## Médicos

**Requisitos Funcionais**

- O médico poderá se cadastrar com os seguintes dados: nome, e-mail, senha, duração da consulta;
- O médico poderá fazer o login na API utilizando o e-mail e a senha;
- O médico poderá fazer o logout na API;

**Requisitos Não Funcionais**

- A senha deve ser cadastrada de forma criptografada;
- O acesso deve ser permitido utilizando token JWT;

**Regras de Negócio**

- O nome do médico deve ter no máximo 100 caracteres;
- O senha do médico deve ter no mínimo 6 caracteres;
- O e-mail do médico deve ter no máximo 250 caracteres e deve ser verificado se é um e-mail válido;
- A duração da consulta deve ser cadastrada em minutos;

## Pacientes

**Requisitos Funcionais**

- O médico poderá cadastrar pacientes com os seguintes dados: nome, telefone, e-mail, data de nascimento, sexo, altura e peso;
- O médico poderá listar os pacientes cadastrados por ele;
- O médico poderá editar os pacientes cadastrados por ele;
- O médico poderá optar por remover apenas os dados que identificam o paciente, seguindo regras da LGPD;

**Regras de Negócio**

- O nome do paciente deve ter no máximo 100 caracteres;
- O telefone do paciente é obrigatório, deve ter 14 caracteres e deve ser cadastrado sem nenhum tipo de formatação;
- O e-mail do paciente deve ter no máximo 250 caracteres e deve ser verificado se é um e-mail válido;
- A data de nascimento do paciente deve ser verificada para ter certeza de que é uma data válida;
- O sexo do paciente deve aceitar os valores 'M' e 'F';
- A altura deve ser cadastrada em centímetros e deve ser validado para ter no máximo 3 dígitos numéricos;
- O peso do paciente deve ser validado para ter no máximo 3 dígitos antes da vírgula e 1 dígito depois da vírgula;
- Na listagem dos pacientes devem aparecer todos os dados do paciente;
- Na visualização de um paciente deverá mostrar os seguintes dados: nome, data de nascimento, sexo, telefone, altura, peso, listagem das consultas do paciente (data da consulta e anotações);

## Consultas

**Requisitos Funcionais**

- O médico poderá cadastrar agendamentos de consultas para seus pacientes com os seguintes dados: paciente, data, hora;
- O médico poderá listar os agendamentos de consultas de seus pacientes;
- O médico poderá editar os agendamentos de consultas de seus pacientes;
- O médico poderá excluir os agendamentos de consultas de seus pacientes;
- O médico poderá cadastrar anotações durante a consulta com seus pacientes;
- O médico poderá visualizar as anotações das consultas de seus pacientes;

**Regras de Negócio**

- A data e o horário da consulta deve ser cadastrada no formato ISO 8601. Exemplo: 2021-09-02 21:00:00;
- O médico não poderá cadastrar mais de um paciente no mesmo horário;
- O médico não poderá cadastrar mais de um paciente no mesmo intervalo de tempo. Deverá respeitar a duração da consulta cadastrada pelo médico;
- Na listagem das consultas devem aparecer os seguintes dados: nome do paciente, data da consulta, hora da consulta;

## API Rest

**Requisitos Não Funcionais**

> Obrigatórios
- A API deve usar o padrão de API REST (HTTP/JSON);
- Pode ser feito em node.js (javascript ou typescript) ou PHP (laravel);
- Documentação da interface da API gerada (swagger, open-api, RAML ou postman);
- Os dados devem ser validados (existência e formatos) na inserção/atualização para garantir consistência da base;
- Implementar testes unitários e/ou de integração e/ou documentação de testes (casos de teste / script de teste);

> Desejáveis
- Documentação da modelagem do banco de dados (diagrama ER ou de classe);
- Para o banco de dados pode usar MySQL ou PostgreSQL, podendo optar ou não pelo uso de ORM;
- Setup de ambiente de desenvolvimento usando docker / docker-compose;
- Hospedar em um ambiente cloud a sua escolha (Heroku, AWS EBS, IBM Cloud, etc);
- Garantir autenticação e/ou autorização (login/logout, token JWT, roles);
- Implementar alguma ferramenta de lint ou qualidade (sonar, code-quality, eslint, etc);
- Deploy automatizado via pipeline (gitlab-ci, bitbucket pipeline, github actions, etc);
